-- Cifrado Cesar version modulo 26

-- Copyright (C) 2019  Vicente parra hurtado - NukeBull

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>

import Data.Char

-- Cramos las funciones para el desplazamieto del cifrado cesar mod 26

minusculas :: Int -> Char -> Char
minusculas n c =  chr ( ( mod ( (ord c) - (ord 'a') + n ) 26 ) + (ord 'a') )


mayusculas :: Int -> Char -> Char
mayusculas n c =  chr ( ( mod ( (ord c) - (ord 'A') + n ) 26 ) + (ord 'A') )

-- Uso las funciones para mayuscula y minusculas el resto carecteres igual.

desplaza :: Int -> Char -> Char
desplaza n c
   | elem c ['a'..'z'] = (minusculas n c)
   | elem c ['A'..'Z'] = (mayusculas n c)
   |otherwise          = c

 
-- Implementamos para String  

cesar :: Int -> String -> String
cesar n xs = [desplaza n x | x <- xs]

main = do 
   
  putStrLn "Introduce el texto a cifrar"
  line <- getLine
  putStrLn "Introduce el número de desplazamientos (3 para el Cesar Original)"
  numero <- getLine
-- num numero desplazamientos 
-- texto  texto cifrado texroDes texto descifrado
  let num = read (numero)
  let texto = cesar num line
  let textoDes = cesar (-num) texto
  putStrLn ("Cifro hacía la derecha moviendo " ++ (show(numero)) ++ " posiciones")
  putStrLn ( show ( texto))
  putStrLn ("Descifro hacía la izquierda moviendo " ++ show(numero) ++ " posiciones")
  putStrLn ( show ( textoDes))






    
