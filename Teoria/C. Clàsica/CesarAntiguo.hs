-- Cifrado Cesar latín antiguo versión modulo 23 con 3 desplazamientos.

-- Copyright (C) 2019  Vicente Parra Hurtado - NukeBull

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>

-- Alfabeto Latín Antiguo
-- En la Roma Antigua solo teníamos mayúsculas y las letras J,U,Ñ,W no estaban aún en el alfabeto.
-- La J fue I y la U fue V ( JESUS era IESVS ).
-- X e Y entraron en siglo 2 a.C. por la conquista del Imperio Griego.
-- Las minúsculas llegaron en la Edad Media.
-- La Ñ aparece como sustitución del dígrafo "NN" al final de la Edad Media en algunos alfabetos derivados del latín como el Castellano.
-- W entra en el siglo XIII por influecia de las lenguas germánicas.
-- J,U no llegan como tales hasta el siglo XVIII, aunque existieron desde el origen ligadas a las letras I y V.


import Data.Char

-- Creamos el alfabeto Latín Antiguo

alfabeto ::  [Char]
alfabeto = ['A'..'I']++['K'..'T']++['V']++['X'..'Z']

-- Cramos las funciones para el desplazamiento del cifrado cesar mod 23. 
-- Adaptando ASCII al latín Antiguo.
-- La J fue I y la U fue V durante La epoca Romana ( JULIO CESAR era IULIO CESAR), se corregirá el texto descifrado.
-- Las minúsculas , la Ñ y la W se consideran no validas, se convertirán en *


antiguas :: Int -> Char -> Char
antiguas  n c 
   | elem c ['A'..'I'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n ) 23)
   | elem c ['J'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n-1 ) 23)
   | elem c ['K'..'T'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n-1 ) 23)
   | elem c ['U'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n-1 ) 23)
   | elem c ['V'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n-2 ) 23)
   | elem c ['X'..'Z'] = alfabeto !! (mod ( (ord c) - (ord 'A') + n-3) 23)
   | elem c [' '] = ' '
   |otherwise          = '*'

-- Implementamos para String. 

cesar :: Int -> String -> String
cesar n xs = [antiguas  n x | x <- xs]

main = do 
  
  -- Mostramos el Alfabeto Latin Antiguo.
  
  putStrLn "Alfabeto latín antiguo"
  putStrLn ( show ( alfabeto)) 

-- Pedimos el texto a cifrar.

  putStrLn "Introduce el texto a cifrar (Los caracteres que no existían en el latín antiguo se transformaran en *) "
  line <- getLine

-- Ciframos y deciframos n=3 y n=-3
-- El cifrado original cesar es de 3 desplazamientos.

  let texto = cesar 3 line
  let textoDes = cesar (-3) texto
  putStrLn ("Cifro hacía la derecha moviendo 3 posiciones")
  putStrLn ( show ( texto))
  putStrLn ("Descifro hacía la izquierda moviendo 3 posiciones corrigiendo J y U ")
  putStrLn ( show ( textoDes))   
