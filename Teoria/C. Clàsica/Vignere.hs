-- Cifrado Cesar-Vignere version modulo 26 alfabeto Ingles

import Data.Char

-- Creamos las funciones para el desplazamieto del cifrado cesar mod 26

minusculas :: Int -> Char -> Char
minusculas n c =  chr ( ( mod ( (ord c) - (ord 'a') + n ) 26 ) + (ord 'a') )


mayusculas :: Int -> Char -> Char
mayusculas n c =  chr ( ( mod ( (ord c) - (ord 'A') + n ) 26 ) + (ord 'A') )

-- Uso las funciones para mayuscula y minusculas el resto carecteres igual.

desplaza :: Int -> Char -> Char
desplaza n c
   | elem c ['a'..'z'] = (minusculas n c)
   | elem c ['A'..'Z'] = (mayusculas n c)
   |otherwise          = c

-- funcion para generar patron númerico para clave Vigenére   
clave :: Char -> Int
clave c 
   | elem c ['a'..'z'] = ((ord c) - (ord 'a'))
   | elem c ['A'..'Z'] = ((ord c) - (ord 'A'))
   |otherwise          = 0


-- Implementamos para String  

cesar :: Int -> String -> String
cesar n xs = [desplaza n x | x <- xs]

-- Implemeto Vigenére para desplazamintos basados en una clave (polialfabético)

vigenere ::  [Int] -> String -> String
vigenere n c = zipWith (desplaza) n c


main = do 

 -- pedimos los datos necesarios   
  putStrLn "Introduce el texto a codificar"
  line <- getLine

  putStrLn "Introduce el texto de clave"
  pass <- getLine


 -- Calculos para generar una clave mayor que el texto pasando de Char a Int
  let ltext = length line
  let lkey = length pass
  let key1 = [clave x | x <- pass]
  let aumento = ((div ltext lkey) +1) * lkey
  let key = take aumento (cycle key1)

-- Cifrado Cesar estandar de 3 desplazamientos
  let num = 3

-- Cesar cifrar-descifrar funcion + 3 + texto
  let texto = cesar num line
  let textoDes = cesar (-num) texto

-- cifrado Vigenére funcion + clave + texto
  let textVig = vigenere key line

-- Descifrando Vigenére y generando clave inversa
  let keyDes = map (* (-1)) key
  let textVigDes = vigenere keyDes textVig

-- Datos por Pantalla para curso
  putStrLn (" (Cesar) Cifro hacía la derecha moviendo 3 posiciones")
  putStrLn ( show ( texto))
  putStrLn (" (Cesar) Descifro hacía la izquierda moviendo 3 posiciones")
  putStrLn ( show ( textoDes))
  putStrLn (" (Vignere) Clave y desplazamientos equivalentes despues de ajustes (Cifrar -Descifrar")
  putStrLn ( show ( pass))
  putStrLn ( show ( key))
  putStrLn ( show ( keyDes))
  putStrLn (" (Vigenére) Cada letra de la clave equivale a un alfabeto-desplazamiento distinto")
  putStrLn ( show ( textVig))
  putStrLn (" (Vigenére) desplazamos en sentido contrario")
  putStrLn ( show ( textVigDes))






    
