-- www.nukebull.com
-- Escilata. 


-- Definimos una matriz m * n
matriz :: Int -> Int -> [(Int,Int)]
matriz x y = [(m,n) | m <- [1..x] , n <- [1..y]]

-- Enumeramos las celdas de la matriz
matrizOrd :: Int -> Int -> [Int]
matrizOrd x y = [((m-1) * y) + n | m <- [1..x] , n <- [1..y]]

-- Definimos la matriz transpuesta
matrizT :: Int -> Int -> [(Int,Int)]
matrizT x y = [(n,m) | m <- [1..y] , n <- [1..x]]

-- Enumeramos las celdas de la matriz traspuesta
-- en referencia a la matriz original
matrizTOrd :: Int -> Int -> [Int]
matrizTOrd x y= [((n-1) * y) + m | m <- [1..y] , n <- [1..x]]

-- sacamos cada uno de los valores para el cifrado de la Esciláta
numeroOrd :: Int -> Int -> Int -> Int
numeroOrd m n x = matrizTOrd m n !! x

main =  do

-- Pedimos los espacios que tienen la vara
    putStrLn "Introduce un entero con el valor del grosor de la vara"
    vara <- getLine
    let m = read (vara)

--  Pedimos el texto a cifrar 
    putStrLn $ "Introduce texto para una vara con "++(show(m))++" espacios de grosor"
    line <- getLine
    let fin = length line
    putStrLn $ "\n Numero de caracteres " ++ (show(fin))

-- Calculamos en que matriz cabe y rellenamos espacios con letras
   
    let n  = if mod fin m  == 0 
               then   (div fin m) 
               else   (div fin m) + 1

-- Rellenamos espacios
    let num =  n*m - fin
    let texto = line ++ [' ' | x <- [1..num] ]


-- Mostramos el texto con relleno
    putStrLn "\n El texto enrrollado"
    putStrLn (show(texto))

-- Mostramos el texto descifrado
    let textoCifrado = [ texto !! ((numeroOrd m n x)-1) | x <-[0..(m*n)-1] ]
    putStrLn "\n El texto desenrrollado"
    putStrLn (show (textoCifrado))

-- Mostramos los datos de las Matrices 
    putStrLn $ "\n Tamaño de la matriz: "++(show (m))++" filas por "++(show (n))++" columnas, "++(show(m*n))++" celdas\n"
    putStrLn $ "\n Matriz inicial \n" ++  (show (matriz m n))
    putStrLn $ "\n Orden M. inicial \n" ++ (show (matrizOrd m n))
    putStrLn $ "\n Tamaño de la matriz transpuesta: "++(show (n))++" filas por "++(show (m))++" columnas, "++(show(m*n))++" celdas\n"
    putStrLn $ "\n Matriz Transpuesta \n" ++ (show (matrizT m n))
    putStrLn $ "\n Orden M. Transpuesta \n" ++ (show (matrizTOrd m n))