
import System.Environment
import Crypto.Hash
import qualified Data.ByteString as S

hashing :: S.ByteString -> IO ()
hashing msg = do
    putStrLn $ "     MD5 = " ++ show (hashWith MD5    msg)
    putStrLn $ "    sha1 = " ++ show (hashWith SHA1   msg)
    putStrLn $ "  sha256 = " ++ show (hashWith SHA256 msg)
    putStrLn $ "  sha384 = " ++ show (hashWith SHA384 msg)
    putStrLn $ "  sha512 = " ++ show (hashWith SHA512 msg)
    putStrLn $ "SHA3_256 = " ++ show (hashWith SHA3_256 msg)
    putStrLn $ "SHA3_384 = " ++ show (hashWith SHA3_384 msg)
    putStrLn $ "SHA3_512 = " ++ show (hashWith SHA3_512 msg)


main = do
    (fileName1:_) <- getArgs
    contents <- S.readFile fileName1
    hashing  contents

