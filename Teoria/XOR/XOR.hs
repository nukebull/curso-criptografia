-- Copyright (C) 2019  Vicente parra hurtado - NukeBull

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>

-- Cifrado XOR para textos desde terminal.

import Data.Bits
import Data.Char

-- funciones: texto a numero ascii -> suma xor -> ASCII a String

textoInt :: String -> [Int]
textoInt c = map(ord) c

cifrar :: [Int] -> [Int] -> [Int]
cifrar a b = zipWith (xor) a b

textoString :: [Int] -> String
textoString n = map (chr) n


main = do 
    -- Introducimos datos por terminal Texto y clave

    putStrLn "Texto para Cifrar"
    line <- getLine
    putStrLn "Clave de Cifrado"
    clave <- getLine

    -- Hacemos la clave del mismo tamaño que el texto
    let linelength = length (line)
    putStrLn "\n Caracteres del texto"
    putStrLn (show(linelength))
    let newclave = take linelength (cycle (clave))  
    putStrLn "\n Clave extendida"
    putStrLn (show(newclave))
    let clavelength = length (newclave)
    putStrLn "\n Caracteres de la clave extendida"
    putStrLn (show(clavelength))

    -- Calculamos la lista ascii de los String texto y clave
    let mensajeInt = textoInt line
    putStrLn "\n Lista ASCII Mensaje"
    putStrLn (show(mensajeInt))

    let claveInt = textoInt newclave
    putStrLn "\n Lista ASCII de la clave extendida"
    putStrLn (show(claveInt))

    -- ciframos  (1 cifra, 2 concvierte a Char)

    let cifradoInt = cifrar mensajeInt claveInt
    putStrLn "\n Lista ASCII del mensaje Cifrado"
    putStrLn (show(cifradoInt))

    let mensaje = textoString cifradoInt
    putStrLn "\n Mensaje Cifrado"
    putStrLn (show(mensaje))

    -- Desciframos volviendo hace XOR con la misma clave ( 1 descifra, 2 concvierte a Char )

    let dobleXORInt = cifrar cifradoInt  claveInt
    putStrLn "\n Lista ASCII del mensaje Descifrado"
    putStrLn (show(dobleXORInt))

    let dobleXOR = textoString dobleXORInt
    putStrLn "\n Mensaje Descifrado"
    putStrLn (show(dobleXOR))