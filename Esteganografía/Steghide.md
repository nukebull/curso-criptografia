### Algoritmos disponibles
~~~
steghide –encinfo
~~~
### Tamaño disponible para poder esconder el mensaje.
~~~
steghide –info IMAGEN.jpg
~~~
### Como insertar mensaje
~~~
steghide embed  -cf IMAGEN.jpg -ef Mensaje.txt
~~~
-cf fichero contenedor, -ef fichero embebido

Añadimos algunos argumentos para su funcionamiento

-z para el nivel de compresión (1-9)
-e tipo de algoritmo,
-p contraseña (passphrase).
-v información detallada (verbose).

~~~
steghide embed -e rijndael-256 -z 9 -v -p password -cf IMAGEN.jpg -ef Mensaje.txt
~~~
### Extraer mensaje 

-sf fichero esteganográfico
~~~
esteghide extract -sf IMAGEN.jpg
~~~