
Debemos tener instalado gnupg, zbar y qrencode

(debian...)
sudo apt-get install gnupg zbar-tools qrencode

(Arch ...)
sudo pacman -S gnupg zbar qrencode 

Dar permisos de ejecucion con:
 chmod +x Cifrar.sh 
 chmod +x CifrarTerminal.sh
 chmod +x Descifrar.sh

Ejecucion:
 ./Cifrar.sh fichero.txt
 ./CifrarTrminal.sh
 ./Descifrar.sh ImagenQR.png
