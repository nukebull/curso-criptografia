#!/bin/bash
#Desciframos el codigo QR generando un archivo texto mensaje.txt
# La imagen QR se debe añadir como argumento
# para ejecutar ./Descifrar ImagenQR.png

#leeemos codigoQR y ajustamos formato para gpg con sed
zbarimg  $1 >  date.txt.asc
sed '1 s/QR-Code:-----BEGIN PGP MESSAGE-----/-----BEGIN PGP MESSAGE-----/g' date.txt.asc > date.asc

#desciframos el mensaje con GnuPG
gpg -d date.asc > Mensaje.txt

#Borramos archivos Intermedios
rm date.txt.asc
rm date.asc