# Guión de la Charla de Criptografía. (Alpha 1.2)

- El nivel de la charla es para usuarios de linux sin un conocimiento previo de Criptografía. 
- Haremos una breve introducción a la historia de la criptografía  y nos centraremos en el uso de software para cifrado por bloques 

## 1. Parte Teórica.

Charla con una breve introducción a la criptografía y su manejo con software libre en S.O. Linux.

### 1.1. Introducción y Criptografía Clásica.

- Repaso a la Historia de la criptografía.
- Cifrado Cesar, Vigenère y M. Enigma. 
- Conceptos básicos: cifrar, codificar, ...
- Veremos un algoritmo creado por un compañero.

### 1.2 Clasificación de los criptosistemas. Método de cifrado por bloques y de flujo.
- Daremos un pequeña descripción de cada uno de ellos pero nos centraremos en el software de cifrado por bloques.

#### 1.2.1. Cifrado de flujo.

- Cifrado XOR
- Bases, conceptos y ejemplos.

#### 1.2.2. Criptografía Simétrica.

- Principios de Kerckhoffs
- Bases del cifrado simétrico, fundamentos y la importancia de la clave para los algoritmos modernos.
- La necesidad de  un gestor de contraseñas.
- Ataques comunes para este tipo de cifrado.

#### 1.2.3. Criptografía Asimétrica.

- Matemática discreta
- Teoría de la información
- Bases del cifrado asimétrico, fundamentos y los principales algoritmos.
- Como afectan los ataques conocidos a este método de cifrado.
 
#### 1.2.4  Software de cifrado AesCrypt, keepass, GnuPG, Kleopatra y VeraCrypt.

- Aprenderemos algo de estas aplicaciones y romperemos una clave débil por fuerza bruta para ver la importancia de la contraseña en este tipo de cifrado.
- Como usar GnuPG en la version de cifrado asimétrico. y administrando claves con Kleopatra

### 1.3 Funciones hash. Integridad de Archivos
- Como funcionan las funciones hash.
- Tipos mas conocidos.
- Debilidades de los más antiguos.

### 1.4. Esteganografía.

- Esconder mensajes en archivos de mayor tamaño. 
- Ejemplos en archivos de video y audio.

#### 1.4.1. Software Steghide.

- Ocultación de mensajes en Imágenes y archivos de audio.

---

## 2. Parte Práctica.

Para la parte práctica necesitamos tener instalados en nuestra distribución Linux, se recomienda una que esta basada en Debian.

#### 2.0.1 Instalaciones previas en Arch y derivados.

Instalación del software en Arch Linux, Manjaro, Antergos,...

~~~
sudo pacman -Syu
sudo pacman -S gnupg zbar qrencode steghide kleopatra
~~~
Tor Browser:
https://www.torproject.org/projects/torbrowser.html.en

Para Inslar OnionShare se debe usar repositorios AUR


#### 2.0.2 Instalaciones previas en Debian y derivados.

Instalación del software en Debian, Ubuntu,...

~~~
sudo apt-get update
sudo apt-get install gnupg zbar-tools qrencode steghide kleopatra
~~~



Instalar Onion Share y Tor Browser
~~~
sudo add-apt-repository ppa:micahflee/ppa

sudo apt update

sudo apt install -y onionshare
~~~
Tor Browser: Si ya has instalado Onion Share:
~~~
torbrowser-launcher
~~~

#### 2.0.3 Alternativas para Windows

- Keepass2
- GnuPG
- Tor Browser - Onion Share

### 2.1 Práctica de Mensajes Cifrados a traves de redes Sociales y Mensajeria Instantanea.

- Trastearemos con GnuPG, Kleopatra, qrencode (codigos QR) y zbar (Codigos QR)
- Enviaremos mensajes cifrados atraves de twitter, telegram, facebook,....

### 2.2 Paranoico en la Red Tor (Cifrado, TorBrowser, Onion Share)

- Instalación de Tor Browser y Onion Share

- Ante la llegada de la quantum computing vamos a idear un sistema de transferencia de archivos con un limitado periodo de vida. (Nuestra clave asimétrica estaría rota en minutos y el tiempo apremia)

### 2.3 Práctica de esteganografía.

- Trastearemos con steghide virtudes y limitaciones. 
- Imágenes ....
